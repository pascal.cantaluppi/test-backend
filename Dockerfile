# build environment
FROM node:17-alpine as builder
WORKDIR /app
COPY . .
RUN npm i
RUN npm run prebuild
RUN npm run build

# production environment
FROM node:17-alpine 
WORKDIR /app
COPY --from=builder /app/package.json .
COPY --from=builder /app/dist ./dist
COPY --from=builder /app/node_modules ./node_modules
RUN npm i npm@8.3.1 -g
CMD ["npm", "run", "start:prod"]

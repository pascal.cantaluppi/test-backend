-- Table: public.Library

-- DROP TABLE public.Library;

CREATE TABLE public.Library
(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    title character varying(100) COLLATE pg_catalog."default" NOT NULL,
    description character varying(200) COLLATE pg_catalog."default" NOT NULL,
    url character varying(100) COLLATE pg_catalog."default" NOT NULL,
    img character varying(100) COLLATE pg_catalog."default" NOT NULL,
    active boolean NOT NULL,
    CONSTRAINT "PK_Library" PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.Library
    OWNER to postgres;
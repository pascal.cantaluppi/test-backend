import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { Logger } from '@nestjs/common';

require('dotenv').config();

async function bootstrap() {
  const logger = new Logger('bootstrap');
  const app = await NestFactory.create(AppModule);
  const port = process.env.PORT || 3000;

  if (process.env.NODE_ENV === 'development') {
    //console.log('--> NODE_ENV=' + process.env.NODE_ENV);
    app.enableCors();
  }

  const swaggerCustomOptions = {
    customCss: '.swagger-ui section.models { visibility: hidden;}',
  };
  const config = new DocumentBuilder()
    .setTitle('Automated testing')
    .setDescription('Backend API description')
    .setVersion('1.0')
    .addTag('library')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document, swaggerCustomOptions);

  await app.listen(port);
  logger.log(`Nest application listening on port ${port}`);
  //logger.log(`test ${process.env.test}`);
}
bootstrap();

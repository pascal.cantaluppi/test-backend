import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateLibraryDto } from './dto/library.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { LibraryRepository } from './library.repository';
import { Library } from './library.entity';

@Injectable()
export class LibraryService {
  constructor(
    @InjectRepository(LibraryRepository)
    private libraryRepository: LibraryRepository,
  ) {}

  async getLibraries(): Promise<Library[]> {
    return this.libraryRepository.getLibraries();
  }

  async getLibraryById(id: number): Promise<Library> {
    const found = await this.libraryRepository.findOne({
      where: { id },
    });
    if (!found) {
      throw new NotFoundException(`Library with id ${id} not found`);
    } else {
      return found;
    }
  }

  async createLibrary(createLibraryDto: CreateLibraryDto): Promise<Library> {
    return this.libraryRepository.createLibrary(createLibraryDto);
  }

  async updateLibraryTitle(id: number, title: string): Promise<Library> {
    const library = await this.getLibraryById(id);
    library.title = title;
    await library.save();
    return library;
  }

  async deleteLibrary(id: number): Promise<void> {
    if (id > 3) {
      return; // prodect demo data
    } else {
      const result = await this.libraryRepository.delete({ id });
      if (result.affected === 0) {
        throw new NotFoundException(`Library with id ${id} not found`);
      }
    }
  }
}

import { IsNotEmpty } from 'class-validator';

export class CreateLibraryDto {
  @IsNotEmpty()
  title: string;
  @IsNotEmpty()
  description: string;
  @IsNotEmpty()
  url: string;
  @IsNotEmpty()
  img: string;
  @IsNotEmpty()
  active: boolean;
}

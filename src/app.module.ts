import { Module } from '@nestjs/common';
import { LibraryModule } from './library/library.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmConfig } from './config/typeorm.config';

@Module({
  imports: [LibraryModule, TypeOrmModule.forRoot(typeOrmConfig)],
})
export class AppModule {}
